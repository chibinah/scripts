#!/bin/sh

#obligé de réinstaller glibc, ki18n et plasma, sinon il manque des locales
PACKAGES_TO_INSTALL="networkmanager-openvpn mc glibc ki18n plasma discover plasma-desktop systemsettings konsole fish qjackctl steamdeck-kde-presets steamos-customizations-jupiter"
FILES_TO_REMOVE="/usr/share/applications/org.mozilla.firefox.desktop /etc/mc/mc.default.keymap /etc/mc/mc.emacs.keymap /etc/mc/mc.ext.ini"

kdialog --warningyesno "Désactivation du mode lecture seule (RO) de l'OS.\nPréparation de Pacman et installation des paquets.\nLe mot de passe utilisateur (deck, pour sudo) sera demandé.\n Continuer ?" --yes-label "Continuer" --no-label "Annuler"

if [ $? = 0 ]; then

    PASSWD=$(kdialog --password "Mot de passe utilisateur steamos (sudo): ")

    echo "$PASSWD" | sudo -S steamos-readonly disable

    kdialog --title "unlock.sh" --passivepopup "Initialisation de Pacman" 10
    echo "$PASSWD" | sudo -S  pacman-key --init
    echo "$PASSWD" | sudo -S  pacman-key --populate

    kdialog --title "unlock.sh" --passivepopup "Mise à jour du dépot des paquets" 10
    echo "$PASSWD" | sudo -S  pacman -Syu

    kdialog --title "unlock.sh" --passivepopup "Suppression des fichiers demandés" 10
    echo "$PASSWD" | sudo -S rm $FILES_TO_REMOVE

    kdialog --title "unlock.sh" --passivepopup "Installation des paquets demandés" 10
    echo "$PASSWD" | sudo -S pacman -S --noconfirm $(echo $PACKAGES_TO_INSTALL)

    kdialog --title "unlock.sh" --passivepopup "Regénération des locales" 10
    echo "$PASSWD" | sudo -S  sed -i 's/#fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/g' /etc/locale.gen
    echo "$PASSWD" | sudo -S locale-gen
    

    kdialog --title "unlock.sh" --passivepopup "Copie des paramètres des limites pour RPCS3" 10
    echo "$PASSWD" | sudo cp /home/deck/bin/SteamOS/rpcs3.conf /etc/security/limits.d/

    kdialog --title "unlock.sh" --passivepopup "Copie de la liste noire pour le SDR" 10
    echo "$PASSWD" | sudo cp /home/deck/bin/SteamOS/blacklist-rtl-for-sdr.conf /etc/modprobe.d/

    kdialog --title "unlock.sh" --passivepopup "Installation terminée" 10

    unset PASSWD

fi
