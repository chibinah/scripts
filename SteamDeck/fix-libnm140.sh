#!/bin/bash
sudo pacman --cachedir /tmp -Sw extra-main/libnm
for f in /usr/lib/libnm.so* ; do sudo mv -v "$f" "${f}.orig" ; done
sudo tar -xvpf /tmp/libnm-*.pkg.tar.zst --wildcards -C / 'usr/lib/libnm.so*'
