#!/bin/sh

PACKAGES_TO_INSTALL="networkmanager-openvpn mc"

kdialog --warningyesno "Désactivation du mode lecture seule (RO) de l'OS.\nPréparation de Pacman et installation des paquets.\nLe mot de passe utilisateur (deck, pour sudo) sera demandé.\n Continuer ?" --yes-label "Continuer" --no-label "Annuler"

if [ $? = 0 ]; then

    PASSWD=$(kdialog --password "Mot de passe utilisateur steamos (sudo): ")

    echo "$PASSWD" | sudo -S steamos-readonly disable

    kdialog --title "unlock.sh" --passivepopup "Initialisation de Pacman" 10
    echo "$PASSWD" | sudo -S  pacman-key --init
    echo "$PASSWD" | sudo -S  pacman-key --populate

    kdialog --title "unlock.sh" --passivepopup "Mise à jour du dépot des paquets" 10
    echo "$PASSWD" | sudo -S  pacman -Syu

    kdialog --title "unlock.sh" --passivepopup "Installation des paquets demandés" 10
    echo "$PASSWD" | sudo -S pacman -S --noconfirm $(echo $PACKAGES_TO_INSTALL)

    kdialog --title "unlock.sh" --passivepopup "Installation terminée" 10

    unset PASSWD

fi
