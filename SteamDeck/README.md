Scripts pour SteamDeck
======================

Quelques scripts et fichiers que j'utilise avec le Steam Deck, en mode bureau KDE/Plasma.

Ces fichiers sont situé dans ~/bin/SteamOS sur mon SteamDeck.


steamos-unlock-prepare-pacman-simple.sh
---------------------------------------

Script qui désactive le mode lecture seule du système, prépare le gestionnaire de paquets pacman, et installe l'extension Open VPN pour Network Manager et Midnight Commander.

Avant d'exécuter le script, définir un mot de passe pour le compte deck (lancer Konsole, et à l'invite de commande, taper passwd, puis suivre les instructions à l'écran).


steamos-unlock-prepare-pacman.sh
--------------------------------

Script similaire à steamos-unlock-prepare-pacman-simple.sh, mais effectuant nettement plus de trucs.

* installation/Réinstallation d'un bon nombre de paquets, pour avoir l'environnement de bureau en français ;

* suppression de fichiers chiants ou bloquant l'installation, ajout et génération de la locale fr_FR.UTF-8 en plus de la locale en_US par défaut (et requis pour Steam/SteamOS) ;

* modification des limites, requis pour RPCS3

* ajout des modules rtl/dvb en liste noire, requis pour gqrx (SDR/SWL)


fix-libnm140.sh
---------------

Correctif pour SteamOS 3.5 (encore en pré-version au 18 août 2023), pour résoudre un conflit de paquets entre ceux proposés par Valve et ceux proposé par Archlinux.

Sans ce correctif, l'accès aux paramètres de Network Manager n'est plus possible depuis "Configuration du système" -> Réseau -> Connexions.

Voir https://social.nah.re/@alex/110910090394002139 et https://github.com/ValveSoftware/SteamOS/issues/1108


rpcs3.conf
----------

Fichier qui sera copié vers /etc/security/limits.d/ via le script steamos-unlock-prepare-pacman.sh.

Modifications des limites du système, requis pour RPCS3.


blacklist-rtl-for-sdr.conf
--------------------------

Liste des modules à ne pas charger quand je branche mon récepteur SDR. Sinon, le récepteur ne sera pas reconnu par GQRX.
