Scripts pour mon Tablet PC
==========================

Linux logo
----------

* Fichier : kirby.pl
* Article : https://blog.chibi-nah.fr/linuxlogo


Gestion de la rotation avec un Tablet PC HP TM2 (Sous Linux)
------------------------------------------------------------

* Fichier : rotate
* Article : https://blog.chibi-nah.fr/gestion-de-la-rotation-avec-un-tablet-pc


VGA_Switcheroo (ou le changement de carte graphique à chaud sous GNU/Linux)
---------------------------------------------------------------------------

* Fichiers : vga_switch, vga_switch-init
* Article : https://blog.chibi-nah.fr/vga-switcheroo


HP TouchSmart TM2
-----------------

* Fichiers : backlight, determine, handler_sh, launch-wicd-gtk
* Article : https://blog.chibi-nah.fr/hp-touchsmart-tm2


Astuces en vrac
---------------

* Fichier : redefResolution
* Article : https://blog.chibi-nah.fr/astuces-en-vrac


Redshift
--------

* Fichier : redshift.conf
* Article : soit un article en vrac, soit une discussion sur IRC.
* Description : Fichier de configuration pour Redshift sous GNU/Linux, avec quelques coordonnées longitude et latitudes simples pour la France (en gros, les 6 principales zones Nord-Ouest, Nord-Est, Centre, Sud-Ouest, Sud-Est, Île-de-France).
